package epos_common

type AccessDao struct {
	Config Configuration
}

func (dao *AccessDao) GetList() []Access {
	var commonDao = CommonDao{
		conf: dao.Config,
	}
	var accessList []Access
	var loggerPrefix = "AccessDao/GetList"

	db := commonDao.OpenConnection()
	defer commonDao.CloseConnection(db)

	rows, err := db.Query("select token,userId,lastLogin,validPeriod from access")
	CheckAndLogError(err, loggerPrefix)
	defer func() {
		closeErr := rows.Close()
		CheckAndLogError(closeErr, loggerPrefix)
	}()

	for rows.Next() {
		access := Access{}
		err := rows.Scan(&access.Token, &access.UserId, &access.LastLogin, &access.ValidPeriod)
		CheckAndLogError(err, loggerPrefix)
		accessList = append(accessList, access)
	}
	return accessList
}

func (dao *AccessDao) GetListByUser(userId uint64) []Access {
	var commonDao = CommonDao{
		conf: dao.Config,
	}
	var accessList []Access
	var loggerPrefix = "AccessDao/GetListByUser"

	db := commonDao.OpenConnection()
	defer commonDao.CloseConnection(db)

	rows, err := db.Query("select token,userId,lastLogin,validPeriod from access where userId = $1", userId)
	CheckAndLogError(err, loggerPrefix)
	defer func() {
		closeErr := rows.Close()
		CheckAndLogError(closeErr, loggerPrefix)
	}()

	for rows.Next() {
		access := Access{}
		err := rows.Scan(&access.Token, &access.UserId, &access.LastLogin, &access.ValidPeriod)
		CheckAndLogError(err, loggerPrefix)
		accessList = append(accessList, access)
	}
	return accessList
}

func (dao *AccessDao) GetItem(token string) Access {
	var commonDao = CommonDao{
		conf: dao.Config,
	}
	var access Access
	var loggerPrefix = "AccessDao/GetItem"

	db := commonDao.OpenConnection()
	defer commonDao.CloseConnection(db)

	row, err := db.Query("select token,userId,lastLogin,validPeriod from access where token = $1", token)
	CheckAndLogError(err, loggerPrefix)
	defer func() {
		closeErr := row.Close()
		CheckAndLogError(closeErr, loggerPrefix)
	}()

	err2 := row.Scan(&access.Token, &access.UserId, &access.LastLogin, &access.ValidPeriod)
	CheckAndLogError(err2, loggerPrefix)

	return access
}

func (dao *AccessDao) CreateItem(access Access) bool {
	var commonDao = CommonDao{
		conf: dao.Config,
	}
	var loggerPrefix = "AccessDao/CreateItem"

	db := commonDao.OpenConnection()
	defer commonDao.CloseConnection(db)

	if access.Token == "" {
		access.Token = CreateToken()
	}

	result, err := db.Exec("insert into access values ($1, $2, $3, $4)",
		access.Token, access.UserId, access.LastLogin, access.ValidPeriod)
	CheckAndLogError(err, loggerPrefix)
	return CheckAffected(result, 1)
}

func (dao *AccessDao) UpdateItem(access Access) bool {
	var commonDao = CommonDao{
		conf: dao.Config,
	}
	var loggerPrefix = "AccessDao/UpdateItem"

	db := commonDao.OpenConnection()
	defer commonDao.CloseConnection(db)

	result, err := db.Exec("update access set userId=$1,lastLogin=$2,validPeriod=$3 where token = $4",
		access.UserId, access.LastLogin, access.ValidPeriod, access.Token)
	CheckAndLogError(err, loggerPrefix)
	return CheckAffected(result, 1)
}

func (dao *AccessDao) DeleteItem(token string) bool {
	var commonDao = CommonDao{
		conf: dao.Config,
	}
	var loggerPrefix = "AccessDao/DeleteItem"

	db := commonDao.OpenConnection()
	defer commonDao.CloseConnection(db)

	result, err := db.Exec("delete from access where token = $1", token)
	CheckAndLogError(err, loggerPrefix)
	return CheckAffected(result, 1)
}
