package epos_common

import (
	"github.com/PuerkitoBio/goquery"
	"io/ioutil"
	"net/http"
)

/* Load goquery Document by page URL
 */
func ParseRequest(parseUrl string) *goquery.Document {
	response := getParserResponse(parseUrl)
	defer response.Body.Close()

	return getDocumentFromResponse(response)
}

func getParserResponse(url string) *http.Response {
	resp, err := http.Get(url)

	SimpleErrorCheck(err)

	return resp
}

/* Load HTML code by page URL
 */
func GetHtml(parseUrl string) string {
	response := getParserResponse(parseUrl)
	bodyBytes, err := ioutil.ReadAll(response.Body)

	SimpleErrorCheck(err)
	bodyString := string(bodyBytes)

	return bodyString
}

func getDocumentFromResponse(resp *http.Response) *goquery.Document {
	doc, err := goquery.NewDocumentFromReader(resp.Body)

	SimpleErrorCheck(err)

	return doc
}
