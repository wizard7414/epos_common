package epos_common

type AccessTypeDao struct {
	Config Configuration
}

func (dao *AccessTypeDao) GetList() []AccessType {
	var commonDao = CommonDao{
		conf: dao.Config,
	}
	var typeList []AccessType
	var loggerPrefix = "AccessTypeDao/GetList"

	db := commonDao.OpenConnection()
	defer commonDao.CloseConnection(db)

	rows, err := db.Query("select id,value,info from access_type")
	CheckAndLogError(err, loggerPrefix)
	defer func() {
		closeErr := rows.Close()
		CheckAndLogError(closeErr, loggerPrefix)
	}()

	for rows.Next() {
		a := AccessType{}
		err := rows.Scan(&a.Id, &a.Value, &a.Info)
		CheckAndLogError(err, loggerPrefix)

		typeList = append(typeList, a)
	}
	return typeList
}

func (dao *AccessTypeDao) GetItem(id int8) AccessType {
	var commonDao = CommonDao{
		conf: dao.Config,
	}
	var typeItem AccessType
	var loggerPrefix = "AccessTypeDao/GetItem"

	db := commonDao.OpenConnection()
	defer commonDao.CloseConnection(db)

	row, err := db.Query("select id,value,info from access_type where id = $1", id)
	CheckAndLogError(err, loggerPrefix)
	defer func() {
		closeErr := row.Close()
		CheckAndLogError(closeErr, loggerPrefix)
	}()

	err2 := row.Scan(&typeItem.Id, &typeItem.Value, &typeItem.Info)
	CheckAndLogError(err2, loggerPrefix)

	return typeItem
}

func (dao *AccessTypeDao) CreateItem(accessType AccessType) bool {
	var commonDao = CommonDao{
		conf: dao.Config,
	}
	var loggerPrefix = "AccessTypeDao/CreateItem"

	db := commonDao.OpenConnection()
	defer commonDao.CloseConnection(db)

	result, err := db.Exec("insert into access_type values ($1, $2, $3)",
		accessType.Id, accessType.Value, accessType.Info)
	CheckAndLogError(err, loggerPrefix)
	return CheckAffected(result, 1)
}

func (dao *AccessTypeDao) UpdateItem(accessType AccessType) bool {
	var commonDao = CommonDao{
		conf: dao.Config,
	}
	var loggerPrefix = "AccessTypeDao/UpdateItem"

	db := commonDao.OpenConnection()
	defer commonDao.CloseConnection(db)

	result, err := db.Exec("update access_type set value=$1,info=$2 where id = $3",
		accessType.Value, accessType.Info, accessType.Id)
	CheckAndLogError(err, loggerPrefix)
	return CheckAffected(result, 1)
}

func (dao *AccessTypeDao) DeleteItem(id int8) bool {
	var commonDao = CommonDao{
		conf: dao.Config,
	}
	var loggerPrefix = "AccessTypeDao/DeleteItem"

	db := commonDao.OpenConnection()
	defer commonDao.CloseConnection(db)

	result, err := db.Exec("delete from access_type where id = $1", id)
	CheckAndLogError(err, loggerPrefix)
	return CheckAffected(result, 1)
}
