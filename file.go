package epos_common

import (
	"bufio"
	"fmt"
	"os"
)

// readLines reads a whole file into memory
// and returns a slice of its lines.
func ReadLinesFromFile(path string) (lines []string, processError error) {
	file, processError := os.Open(path)
	if processError != nil {
		return
	}
	defer func() {
		closeError := file.Close()
		if closeError != nil {
			processError = closeError
		}
	}()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	if scanner.Err() != nil {
		processError = scanner.Err()
	}

	return
}

// writeLines writes the lines to the given file.
func WriteLinesToFile(lines []string, path string) (processError error) {
	file, processError := os.Create(path)
	if processError != nil {
		return
	}
	defer func() {
		closeError := file.Close()
		if closeError != nil {
			processError = closeError
		}
	}()

	w := bufio.NewWriter(file)
	for _, line := range lines {
		_, processError = fmt.Fprintln(w, line)
	}
	return w.Flush()
}
