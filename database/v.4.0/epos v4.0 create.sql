/*	Доменная сущность - Пользователь
*/
CREATE TABLE IF NOT EXISTS app_user (
    user_id							    bigint PRIMARY KEY,
    user_name						    varchar(100) NOT NULL,
    email							    varchar(256) NOT NULL,
    first_name						    varchar(80),
    sur_name                            varchar(80),
    last_name						    varchar(80),
    encrypted_password		            varchar(256) NOT NULL,
    block_date						    timestamp WITH TIME ZONE,
    suspend_date						timestamp WITH TIME ZONE,
    CONSTRAINT users UNIQUE(user_name, email)
);
/*	Доменная сущность - Роль пользователя
*/
CREATE TABLE IF NOT EXISTS app_role (
    role_code							varchar(6) PRIMARY KEY,
    role_title						    varchar(128) NOT NULL,
    CONSTRAINT roles UNIQUE(role_title)
);
/*	Связь MxN - Роли пользователей
*/
CREATE TABLE IF NOT EXISTS user_role (
    user_id							    bigint REFERENCES app_user ON UPDATE CASCADE ON DELETE SET NULL,
    role_code						    varchar(6) REFERENCES app_role ON UPDATE CASCADE ON DELETE SET NULL,
    PRIMARY KEY (user_id, role_code)
);
/*	Доменная сущность - Операция
*/
CREATE TABLE IF NOT EXISTS operation (
    operation_code						bigint PRIMARY KEY,
    operation_title						varchar(128) NOT NULL,
    CONSTRAINT operations UNIQUE(operation_title)
);
/*	Связь MxN - Операции ролей
*/
CREATE TABLE IF NOT EXISTS permission (
    role_code						    varchar(6) REFERENCES app_role ON UPDATE CASCADE ON DELETE SET NULL,
    operation_code						bigint REFERENCES operation ON UPDATE CASCADE ON DELETE SET NULL,
    PRIMARY KEY (role_code, operation_code)
);
/*	Доменная сущность - Продукт
*/
CREATE TABLE IF NOT EXISTS app_product (
    product_id							bigint PRIMARY KEY,
    user_id						        bigint REFERENCES app_user ON UPDATE CASCADE ON DELETE SET NULL,
    description							varchar(256) NOT NULL,
    end_date						    timestamp WITH TIME ZONE NOT NULL,
    blocked                             boolean NOT NULL,
    template						    boolean NOT NULL,
    price           		            money,
    CONSTRAINT products UNIQUE(description)
);
/*	Доменная сущность - Функция
*/
CREATE TABLE IF NOT EXISTS app_feature (
    feature_code						bigint PRIMARY KEY,
    feature_title						varchar(256) NOT NULL,
    feature_value                       integer,
    CONSTRAINT features UNIQUE(feature_title)
);
/*	Связь MxN - Функции продуктов
*/
CREATE TABLE IF NOT EXISTS app_content (
    product_id						    bigint REFERENCES app_product ON UPDATE CASCADE ON DELETE SET NULL,
    feature_code						bigint REFERENCES app_feature ON UPDATE CASCADE ON DELETE SET NULL,
    PRIMARY KEY (product_id, feature_code)
);
/*	Доменная сущность - Авторизация
*/
CREATE TABLE IF NOT EXISTS access (
    token                               varchar(256) PRIMARY KEY,
    user_id							    bigint REFERENCES app_user ON UPDATE CASCADE ON DELETE SET NULL,
    last_login                          timestamp WITH TIME ZONE,
    valid_period                        integer NOT NULL
);
/*	Доменная сущность - Пользовательская коллекция
*/
CREATE TABLE IF NOT EXISTS collection (
    collection_id                       bigint PRIMARY KEY,
    owner							    bigint NOT NULL REFERENCES app_user ON UPDATE CASCADE ON DELETE SET NULL,
    shared                              boolean,
    collection_title                    varchar(256) NOT NULL
);
/*	Связь MxN - Доступ для добавления в коллекцию
*/
CREATE TABLE IF NOT EXISTS commit_access (
    user_id						        bigint REFERENCES app_user ON UPDATE CASCADE ON DELETE SET NULL,
    collection_id						bigint REFERENCES collection ON UPDATE CASCADE ON DELETE SET NULL,
    PRIMARY KEY (user_id, collection_id)
);
/*	Доменная сущность - Конфигурация
*/
CREATE TABLE IF NOT EXISTS setting (
    setting_id                          bigint PRIMARY KEY,
    owner       					    bigint NOT NULL REFERENCES app_user ON UPDATE CASCADE ON DELETE SET NULL,
    setting_code                        varchar(64) NOT NULL,
    setting_title                       varchar(128) NOT NULL,
    setting_class                       varchar(6) NOT NULL,
    string_value                        varchar(128),
    number_value                        integer
);
/*	Связь MxN - Настройки коллекций
*/
CREATE TABLE IF NOT EXISTS configuration (
    setting_id					        bigint REFERENCES setting ON UPDATE CASCADE ON DELETE SET NULL,
    collection_id						bigint REFERENCES collection ON UPDATE CASCADE ON DELETE SET NULL,
    PRIMARY KEY (setting_id, collection_id)
);
/*	Доменная сущность - Объект
*/
CREATE TABLE IF NOT EXISTS object (
    object_id                           bigint PRIMARY KEY,
    owner       					    bigint NOT NULL REFERENCES app_user ON UPDATE CASCADE ON DELETE SET NULL,
    object_title                        varchar(128) NOT NULL,
    collection                          bigint NOT NULL REFERENCES collection ON UPDATE CASCADE ON DELETE SET NULL,
    committed                           timestamp WITH TIME ZONE,
    approved                            timestamp WITH TIME ZONE,
    shared                              boolean
);
/*	Доменная сущность - Альтернативное наименование
*/
CREATE TABLE IF NOT EXISTS alias (
    alias_id                            bigint PRIMARY KEY,
    alias_title       					varchar(256) NOT NULL,
    owner       					    bigint NOT NULL REFERENCES app_user ON UPDATE CASCADE ON DELETE SET NULL,
    CONSTRAINT aliases UNIQUE(alias_title)
);
/*	Связь MxN - Псевдонимы объектов
*/
CREATE TABLE IF NOT EXISTS naming (
    object_id					        bigint REFERENCES object ON UPDATE CASCADE ON DELETE SET NULL,
    alias_id						    bigint REFERENCES alias ON UPDATE CASCADE ON DELETE SET NULL,
    PRIMARY KEY (object_id, alias_id)
);
/*	Доменная сущность - Тип параметра
*/
CREATE TABLE IF NOT EXISTS attribute_type (
    type_code                           varchar(6) PRIMARY KEY,
    type_title       					varchar(256) NOT NULL,
    CONSTRAINT attribute_types UNIQUE(type_title)
);
/*	Доменная сущность - Описание параметра
*/
CREATE TABLE IF NOT EXISTS description (
    description_id                      bigint PRIMARY KEY,
    description_title       			varchar(256) NOT NULL,
    owner       					    bigint NOT NULL REFERENCES app_user ON UPDATE CASCADE ON DELETE SET NULL,
    CONSTRAINT descriptions UNIQUE(description_title)
);
/*	Доменная сущность - Параметр
*/
CREATE TABLE IF NOT EXISTS attribute (
    attribute_id                        bigint PRIMARY KEY,
    owner       					    bigint NOT NULL REFERENCES app_user ON UPDATE CASCADE ON DELETE SET NULL,
    type_code                           varchar(6) NOT NULL REFERENCES attribute_type ON UPDATE CASCADE ON DELETE SET NULL,
    description      			        bigint NOT NULL REFERENCES description ON UPDATE CASCADE ON DELETE SET NULL,
    attribute_class                     varchar(6) NOT NULL
);
/*	Связь MxN - Атрибуты объектов
*/
CREATE TABLE IF NOT EXISTS property (
    object_id					        bigint REFERENCES object ON UPDATE CASCADE ON DELETE SET NULL,
    attribute_id						bigint REFERENCES attribute ON UPDATE CASCADE ON DELETE SET NULL,
    PRIMARY KEY (object_id, attribute_id)
);
/*	Доменная сущность - Значение параметра
*/
CREATE TABLE IF NOT EXISTS value (
    value_id                            bigint PRIMARY KEY,
    property                            bigint NOT NULL REFERENCES attribute ON UPDATE CASCADE ON DELETE SET NULL,
    owner       					    bigint NOT NULL REFERENCES app_user ON UPDATE CASCADE ON DELETE SET NULL,
    object_value   					    bigint REFERENCES object ON UPDATE CASCADE ON DELETE SET NULL,
    string_value                        varchar(128),
    number_value      			        integer,
    float_value                         float,
    date_value                          timestamp with time zone,
    money_value                         money,
    link_value                          varchar(512),
    bool_value                          boolean
);
/*	Доменная сущность - Текстовое значение параметра
*/
CREATE TABLE IF NOT EXISTS text_value (
    value_id                            bigint PRIMARY KEY,
    property                            bigint NOT NULL REFERENCES attribute ON UPDATE CASCADE ON DELETE SET NULL,
    owner       					    bigint NOT NULL REFERENCES app_user ON UPDATE CASCADE ON DELETE SET NULL,
    text_value   					    text
);
/*	Доменная сущность - Изображение
*/
CREATE TABLE IF NOT EXISTS image (
    image_id                            bigint PRIMARY KEY,
    owner       					    bigint NOT NULL REFERENCES app_user ON UPDATE CASCADE ON DELETE SET NULL,
    base_url                            varchar(512),
    reserve_url                         varchar(512),
    content_filter                      boolean
);
/*	Связь MxN - Изображения объектов
*/
CREATE TABLE IF NOT EXISTS graph (
    object_id					        bigint REFERENCES object ON UPDATE CASCADE ON DELETE SET NULL,
    image_id						    bigint REFERENCES image ON UPDATE CASCADE ON DELETE SET NULL,
    PRIMARY KEY (object_id, image_id)
);
/*	Связь MxN - Параметры изображений
*/
CREATE TABLE IF NOT EXISTS image_property (
    image_id						    bigint REFERENCES image ON UPDATE CASCADE ON DELETE SET NULL,
    attribute_id						bigint REFERENCES attribute ON UPDATE CASCADE ON DELETE SET NULL,
    PRIMARY KEY (image_id, attribute_id)
);
/*	Доменная сущность - Тип связи
*/
CREATE TABLE IF NOT EXISTS relation_type (
    type_id                             bigint PRIMARY KEY,
    type_title                          varchar(128),
    owner       					    bigint NOT NULL REFERENCES app_user ON UPDATE CASCADE ON DELETE SET NULL
);
/*	Связь MxNxK - Связи объектов по типам
*/
CREATE TABLE IF NOT EXISTS relation (
    base_object						    bigint REFERENCES object ON UPDATE CASCADE ON DELETE SET NULL,
    related_object						bigint REFERENCES object ON UPDATE CASCADE ON DELETE SET NULL,
    relation    						bigint REFERENCES relation_type ON UPDATE CASCADE ON DELETE SET NULL,
    PRIMARY KEY (base_object, related_object, relation)
);
/*	Доменная сущность - Тип классификатора
*/
CREATE TABLE IF NOT EXISTS classifier_type (
    type_id                             bigint PRIMARY KEY,
    type_title                          varchar(256),
    owner       					    bigint NOT NULL REFERENCES app_user ON UPDATE CASCADE ON DELETE SET NULL
);
/*	Доменная сущность - Классификатор
*/
CREATE TABLE IF NOT EXISTS classifier (
    classifier_id                       bigint PRIMARY KEY,
    classifier_title                    varchar(128),
    owner       					    bigint NOT NULL REFERENCES app_user ON UPDATE CASCADE ON DELETE SET NULL,
    classifier_type                     bigint NOT NULL REFERENCES classifier_type ON UPDATE CASCADE ON DELETE SET NULL,
    parent                              bigint REFERENCES classifier ON UPDATE CASCADE ON DELETE SET NULL
);
/*	Связь MxN - Классификаторы объектов
*/
CREATE TABLE IF NOT EXISTS hierarchy (
    object_id						    bigint REFERENCES object ON UPDATE CASCADE ON DELETE SET NULL,
    classifier_id						bigint REFERENCES classifier ON UPDATE CASCADE ON DELETE SET NULL,
    PRIMARY KEY (object_id, classifier_id)
);
/*	Доменная сущность - Тег
*/
CREATE TABLE IF NOT EXISTS tag (
    tag_id                             bigint PRIMARY KEY,
    tag_title                          varchar(64) NOT NULL,
    owner       					   bigint NOT NULL REFERENCES app_user ON UPDATE CASCADE ON DELETE SET NULL
);
/*	Связь MxN - Теги объектов
*/
CREATE TABLE IF NOT EXISTS object_tag (
    object_id						   bigint REFERENCES object ON UPDATE CASCADE ON DELETE SET NULL,
    tag_id						       bigint REFERENCES tag ON UPDATE CASCADE ON DELETE SET NULL,
    PRIMARY KEY (object_id, tag_id)
);
/*	Доменная сущность - Категория
*/
CREATE TABLE IF NOT EXISTS category (
    category_id                        bigint PRIMARY KEY,
    category_title                     varchar(128) NOT NULL,
    owner       					   bigint NOT NULL REFERENCES app_user ON UPDATE CASCADE ON DELETE SET NULL,
    color                              varchar(7),
    parent                             bigint REFERENCES category ON UPDATE CASCADE ON DELETE SET NULL
);
/*	Связь MxN - Категории объектов
*/
CREATE TABLE IF NOT EXISTS object_category (
    object_id						   bigint REFERENCES object ON UPDATE CASCADE ON DELETE SET NULL,
    category_id						   bigint REFERENCES category ON UPDATE CASCADE ON DELETE SET NULL,
    PRIMARY KEY (object_id, category_id)
);