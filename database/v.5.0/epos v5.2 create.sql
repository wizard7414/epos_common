/*	Доменная сущность - Уровень доступа
*/
CREATE TABLE IF NOT EXISTS access_type
(
    id    integer PRIMARY KEY,
    value varchar(3)   NOT NULL,
    info  varchar(512) NOT NULL,
    CONSTRAINT access_types UNIQUE (value, info)
);
/*	Доменная сущность - Пользователь
*/
CREATE TABLE IF NOT EXISTS app_user
(
    id                bigint PRIMARY KEY,
    userName          varchar(100) NOT NULL,
    email             varchar(256) NOT NULL,
    firstName         varchar(80)              DEFAULT '',
    surName           varchar(80)              DEFAULT '',
    lastName          varchar(80)              DEFAULT '',
    accessLevel       integer      NOT NULL REFERENCES access_type ON UPDATE CASCADE ON DELETE RESTRICT,
    encryptedPassword varchar(256) NOT NULL,
    blockDate         timestamp WITH TIME ZONE DEFAULT to_timestamp(0),
    suspendDate       timestamp WITH TIME ZONE DEFAULT to_timestamp(0),
    CONSTRAINT users UNIQUE (userName, email)
);
/*	Доменная сущность - Роль пользователя
*/
CREATE TABLE IF NOT EXISTS app_role
(
    code  varchar(6) PRIMARY KEY,
    title varchar(128) NOT NULL,
    CONSTRAINT roles UNIQUE (title)
);
/*	Связь MxN - Роли пользователей
*/
CREATE TABLE IF NOT EXISTS user_role
(
    userId bigint REFERENCES app_user ON UPDATE CASCADE ON DELETE RESTRICT,
    role   varchar(6) REFERENCES app_role ON UPDATE CASCADE ON DELETE RESTRICT,
    PRIMARY KEY (userId, role)
);
/*	Доменная сущность - Операция
*/
CREATE TABLE IF NOT EXISTS operation
(
    code  bigint PRIMARY KEY,
    title varchar(128) NOT NULL,
    CONSTRAINT operations UNIQUE (title)
);
/*	Связь MxN - Операции ролей
*/
CREATE TABLE IF NOT EXISTS permission
(
    role      varchar(6) REFERENCES app_role ON UPDATE CASCADE ON DELETE RESTRICT,
    operation bigint REFERENCES operation ON UPDATE CASCADE ON DELETE RESTRICT,
    PRIMARY KEY (role, operation)
);
/*	Доменная сущность - Авторизация
*/
CREATE TABLE IF NOT EXISTS access
(
    token       varchar(256) PRIMARY KEY,
    userId      bigint  NOT NULL REFERENCES app_user ON UPDATE CASCADE ON DELETE RESTRICT,
    lastLogin   timestamp WITH TIME ZONE DEFAULT NOW(),
    validPeriod integer NOT NULL
);
/*	Доменная сущность - Отображаемый стиль объекта
*/
CREATE TABLE IF NOT EXISTS style
(
    id          bigint PRIMARY KEY,
    title       varchar(50) NOT NULL,
    owner       bigint      NOT NULL REFERENCES app_user ON UPDATE CASCADE ON DELETE RESTRICT,
    headerSize  integer     DEFAULT 0,
    textSize    integer     NOT NULL,
    headerStyle varchar(50) DEFAULT '',
    textStyle   varchar(50) DEFAULT '',
    headerColor varchar(7)  DEFAULT '',
    textColor   varchar(7)  DEFAULT '',
    cardColor   varchar(7)  DEFAULT '',
    rowColor    varchar(7)  DEFAULT '',
    CONSTRAINT styles UNIQUE (title)
);
/*	Доменная сущность - Этап объекта
*/
CREATE TABLE IF NOT EXISTS object_stage
(
    id    integer PRIMARY KEY,
    owner bigint       NOT NULL REFERENCES app_user ON UPDATE CASCADE ON DELETE RESTRICT,
    style bigint       NOT NULL REFERENCES style ON UPDATE CASCADE ON DELETE SET NULL,
    value varchar(124) NOT NULL,
    info  varchar(512) DEFAULT ''
);
/*	Доменная сущность - Архив (группа) объектов
*/
CREATE TABLE IF NOT EXISTS archive
(
    id          bigint PRIMARY KEY,
    owner       bigint       NOT NULL REFERENCES app_user ON UPDATE CASCADE ON DELETE RESTRICT,
    style       bigint       NOT NULL REFERENCES style ON UPDATE CASCADE ON DELETE SET NULL,
    value       varchar(124) NOT NULL,
    contentPath varchar(512) DEFAULT '',
    info        varchar(512) DEFAULT ''
);
/*	Доменная сущность - Объект
*/
CREATE TABLE IF NOT EXISTS object
(
    id         bigint PRIMARY KEY,
    owner      bigint                   NOT NULL REFERENCES app_user ON UPDATE CASCADE ON DELETE RESTRICT,
    style      bigint                   NOT NULL REFERENCES style ON UPDATE CASCADE ON DELETE SET NULL,
    addDate    timestamp WITH TIME ZONE NOT NULL,
    changeDate timestamp WITH TIME ZONE DEFAULT to_timestamp(0),
    shared     boolean                  DEFAULT false,
    blocked    boolean                  DEFAULT false,
    stage      integer                  NOT NULL REFERENCES object_stage ON UPDATE CASCADE ON DELETE SET NULL,
    archive    bigint                   NOT NULL REFERENCES archive ON UPDATE CASCADE ON DELETE SET NULL
);
/*	Доменная сущность - Тип атрибута объекта
*/
CREATE TABLE IF NOT EXISTS attribute_type
(
    id            integer PRIMARY KEY,
    description   varchar(50) NOT NULL,
    modifiedTable varchar(50) NOT NULL,
    filtered      boolean     NOT NULL,
    grouped       boolean     NOT NULL,
    hierarchy     boolean     NOT NULL,
    autocomplete  boolean     NOT NULL,
    multiValue    boolean     NOT NULL,
    showInTable   boolean     NOT NULL,
    useAsHeader   boolean     NOT NULL,
    rangeValue    boolean     NOT NULL,
    CONSTRAINT attribute_types UNIQUE (description)
);
/*	Доменная сущность - Атрибут объекта
*/
CREATE TABLE IF NOT EXISTS object_attribute
(
    id           bigint PRIMARY KEY,
    title        varchar(128)             NOT NULL,
    owner        bigint                   NOT NULL REFERENCES app_user ON UPDATE CASCADE ON DELETE RESTRICT,
    style        bigint                   NOT NULL REFERENCES style ON UPDATE CASCADE ON DELETE SET NULL,
    type         integer                  NOT NULL REFERENCES attribute_type ON UPDATE CASCADE ON DELETE SET NULL,
    addDate      timestamp WITH TIME ZONE NOT NULL,
    changeDate   timestamp WITH TIME ZONE DEFAULT to_timestamp(0),
    description  varchar(512)             DEFAULT '',
    filtered     boolean                  NOT NULL,
    grouped      boolean                  NOT NULL,
    hierarchy    boolean                  NOT NULL,
    parent       bigint                   NOT NULL REFERENCES object ON UPDATE CASCADE ON DELETE SET NULL,
    autocomplete boolean                  NOT NULL,
    multiValue   boolean                  NOT NULL,
    showInTable  boolean                  NOT NULL,
    useAsHeader  boolean                  NOT NULL,
    rangeValue   boolean                  NOT NULL
);
/*	Доменная сущность - Тип графики
*/
CREATE TABLE IF NOT EXISTS image_type
(
    id    integer PRIMARY KEY,
    value varchar(30) NOT NULL,
    info  varchar(128) DEFAULT '',
    CONSTRAINT image_types UNIQUE (value)
);
/*	Доменная сущность - Графическое значение параметра
*/
CREATE TABLE IF NOT EXISTS image_value
(
    id         bigint PRIMARY KEY,
    owner      bigint       NOT NULL REFERENCES app_user ON UPDATE CASCADE ON DELETE RESTRICT,
    attribute  bigint       NOT NULL REFERENCES object_attribute ON UPDATE CASCADE ON DELETE RESTRICT,
    value      varchar(700) NOT NULL,
    meta       varchar(512) DEFAULT '',
    author     bigint       NOT NULL REFERENCES object ON UPDATE CASCADE ON DELETE SET NULL,
    type       integer      NOT NULL REFERENCES image_type ON UPDATE CASCADE ON DELETE RESTRICT,
    accessType integer      NOT NULL REFERENCES access_type ON UPDATE CASCADE ON DELETE RESTRICT
);
/*	Доменная сущность - Строковое значение параметра
*/
CREATE TABLE IF NOT EXISTS string_value
(
    id        bigint PRIMARY KEY,
    owner     bigint       NOT NULL REFERENCES app_user ON UPDATE CASCADE ON DELETE RESTRICT,
    attribute bigint       NOT NULL REFERENCES object_attribute ON UPDATE CASCADE ON DELETE RESTRICT,
    value     varchar(700) NOT NULL,
    meta      varchar(512) DEFAULT '',
    selected  boolean      DEFAULT false
);
/*	Доменная сущность - Текстовое значение параметра
*/
CREATE TABLE IF NOT EXISTS text_value
(
    id        bigint PRIMARY KEY,
    owner     bigint NOT NULL REFERENCES app_user ON UPDATE CASCADE ON DELETE RESTRICT,
    attribute bigint NOT NULL REFERENCES object_attribute ON UPDATE CASCADE ON DELETE RESTRICT,
    value     text   NOT NULL,
    meta      varchar(512) DEFAULT ''
);
/*	Доменная сущность - Числовое значение параметра
*/
CREATE TABLE IF NOT EXISTS digital_value
(
    id          bigint PRIMARY KEY,
    owner       bigint NOT NULL REFERENCES app_user ON UPDATE CASCADE ON DELETE RESTRICT,
    attribute   bigint NOT NULL REFERENCES object_attribute ON UPDATE CASCADE ON DELETE RESTRICT,
    objectValue bigint REFERENCES object ON UPDATE CASCADE ON DELETE SET NULL,
    intValue    integer      DEFAULT 0,
    moneyValue  money        DEFAULT 0.0,
    floatValue  float8       DEFAULT 0.0,
    flagValue   boolean      DEFAULT false,
    ratingStart integer      DEFAULT 0,
    ratingEnd   integer      DEFAULT 0,
    unit        varchar(10)  DEFAULT '',
    meta        varchar(512) DEFAULT ''
);
/*	Доменная сущность - Временное значение параметра
*/
CREATE TABLE IF NOT EXISTS date_value
(
    id            bigint PRIMARY KEY,
    owner         bigint NOT NULL REFERENCES app_user ON UPDATE CASCADE ON DELETE RESTRICT,
    attribute     bigint NOT NULL REFERENCES object_attribute ON UPDATE CASCADE ON DELETE RESTRICT,
    dateTimeValue timestamp WITH TIME ZONE DEFAULT to_timestamp(0),
    dateValue     date                     DEFAULT '0001-01-01',
    timeValue     time WITH TIME ZONE      DEFAULT make_time(0, 0, 0),
    remindFlag    boolean                  DEFAULT false,
    meta          varchar(512)             DEFAULT ''
);