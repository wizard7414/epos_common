package epos_common

import "database/sql"

type CommonDao struct {
	conf Configuration
}

func (dao *CommonDao) OpenConnection() *sql.DB {
	db, err := sql.Open(dao.conf.Driver, dao.conf.ConnectionString)
	CheckAndLogError(err, "CommonDao/OpenConnection")
	SimpleErrorCheck(err)
	return db
}

func (dao *CommonDao) CloseConnection(db *sql.DB) {
	err := db.Close()
	CheckAndLogError(err, "CommonDao/CloseConnection")
}
