package epos_common

func (role *Role) Exist(roles []Role) bool {
	if len(roles) != 0 {
		for i := 0; i < len(roles); i++ {
			if roles[i].Code == role.Code {
				return true
			}
		}
	}
	return false
}

func RolesDifference(storedRoles []Role, updatedRoles []Role) (addedRoles []Role, deletedRoles []Role) {
	if len(storedRoles) != 0 {
		if len(updatedRoles) != 0 {
			for i := 0; i < len(storedRoles); i++ {
				if !storedRoles[i].Exist(updatedRoles) {
					deletedRoles = append(deletedRoles, storedRoles[i])
				}
			}
			for j := 0; j < len(updatedRoles); j++ {
				if !updatedRoles[j].Exist(storedRoles) {
					addedRoles = append(addedRoles, updatedRoles[j])
				}
			}
		}
	} else {
		addedRoles = updatedRoles
	}
	return addedRoles, deletedRoles
}
