package epos_common

type OperationDao struct {
	Config Configuration
}

func (dao *OperationDao) GetList() []Operation {
	var commonDao = CommonDao{
		conf: dao.Config,
	}
	var operationList []Operation
	var loggerPrefix = "OperationDao/GetList"

	db := commonDao.OpenConnection()
	defer commonDao.CloseConnection(db)

	rows, err := db.Query("select code,title from operation")
	CheckAndLogError(err, loggerPrefix)
	defer func() {
		closeErr := rows.Close()
		CheckAndLogError(closeErr, loggerPrefix)
	}()

	for rows.Next() {
		operation := Operation{}
		err := rows.Scan(&operation.Code, &operation.Title)
		CheckAndLogError(err, loggerPrefix)

		operationList = append(operationList, operation)
	}
	return operationList
}

func (dao *OperationDao) GetListByRole(roleCode string) []Operation {
	var commonDao = CommonDao{
		conf: dao.Config,
	}
	var operationList []Operation
	var loggerPrefix = "OperationDao/GetListByRole"

	db := commonDao.OpenConnection()
	defer commonDao.CloseConnection(db)

	rows, err := db.Query("select operation.code,operation.title from operation o inner join permission p on o.code=p.operation"+
		"where p.role = $1", roleCode)
	CheckAndLogError(err, loggerPrefix)
	defer func() {
		closeErr := rows.Close()
		CheckAndLogError(closeErr, loggerPrefix)
	}()

	for rows.Next() {
		operation := Operation{}
		err := rows.Scan(&operation.Code, &operation.Title)
		CheckAndLogError(err, loggerPrefix)

		operationList = append(operationList, operation)
	}
	return operationList
}

func (dao *OperationDao) GetItem(code uint64) Operation {
	var commonDao = CommonDao{
		conf: dao.Config,
	}
	var operation Operation
	var loggerPrefix = "OperationDao/GetItem"

	db := commonDao.OpenConnection()
	defer commonDao.CloseConnection(db)

	row, err := db.Query("select code,title from operation where code = $1", code)
	CheckAndLogError(err, loggerPrefix)
	defer func() {
		closeErr := row.Close()
		CheckAndLogError(closeErr, loggerPrefix)
	}()

	err2 := row.Scan(&operation.Code, &operation.Title)
	CheckAndLogError(err2, loggerPrefix)

	return operation
}

func (dao *OperationDao) CreateItem(operation Operation) bool {
	var commonDao = CommonDao{
		conf: dao.Config,
	}
	var loggerPrefix = "OperationDao/CreateItem"

	db := commonDao.OpenConnection()
	defer commonDao.CloseConnection(db)

	if operation.Code == 0 {
		operation.Code = CreateEposId()
	}

	result, err := db.Exec("insert into operation values ($1, $2)",
		operation.Code, operation.Title)
	CheckAndLogError(err, loggerPrefix)
	return CheckAffected(result, 1)
}

func (dao *OperationDao) LinkItemWithRole(operationCode uint64, roleCode string) bool {
	var commonDao = CommonDao{
		conf: dao.Config,
	}
	var loggerPrefix = "OperationDao/LinkItemWithRole"

	db := commonDao.OpenConnection()
	defer commonDao.CloseConnection(db)

	result, err := db.Exec("insert into permission values ($1, $2)",
		roleCode, operationCode)
	CheckAndLogError(err, loggerPrefix)
	return CheckAffected(result, 1)
}

func (dao *OperationDao) UpdateItem(operation Operation) bool {
	var commonDao = CommonDao{
		conf: dao.Config,
	}
	var loggerPrefix = "OperationDao/UpdateItem"

	db := commonDao.OpenConnection()
	defer commonDao.CloseConnection(db)

	result, err := db.Exec("update operation set title=$1 where code = $2",
		operation.Title, operation.Code)
	CheckAndLogError(err, loggerPrefix)
	return CheckAffected(result, 1)
}

func (dao *OperationDao) DeleteItem(code uint64) bool {
	var commonDao = CommonDao{
		conf: dao.Config,
	}
	var loggerPrefix = "OperationDao/DeleteItem"

	db := commonDao.OpenConnection()
	defer commonDao.CloseConnection(db)

	result, err := db.Exec("delete from operation where code = $1", code)
	CheckAndLogError(err, loggerPrefix)
	return CheckAffected(result, 1)
}

func (dao *OperationDao) DeleteItemFromRole(operationCode uint64, roleCode string) bool {
	var commonDao = CommonDao{
		conf: dao.Config,
	}
	var loggerPrefix = "OperationDao/DeleteItemFromRole"

	db := commonDao.OpenConnection()
	defer commonDao.CloseConnection(db)

	result, err := db.Exec("delete from permission where role = $1 and operation = $2", roleCode, operationCode)
	CheckAndLogError(err, loggerPrefix)
	return CheckAffected(result, 1)
}
