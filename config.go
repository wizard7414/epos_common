package epos_common

type Configuration struct {
	ConnectionString string
	Driver           string
	LogFile          string
	ValidPeriod      int32
}
