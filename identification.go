package epos_common

import (
	"crypto/rand"
	"fmt"
	"strconv"
	"time"
)

type ObjectType int

const (
	Article ObjectType = iota
	Building
	Character
	Equipment
	Aircraft
	Collection
	Person
	Technics
	Weapon
)

var object = [...]string{"A", "B", "C", "E", "I", "O", "P", "T", "W"}
var month = [...]string{"A", "B", "C", "D", "E", "F", "J", "H", "I", "J", "K", "L"}
var hour = [...]string{"A", "B", "C", "D", "E", "F", "J", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R",
	"S", "T", "U", "V", "W", "X"}
var method = [...]string{"H", "A"}

func CreateEposId() uint64 {
	moment := time.Now()
	milliseconds := int64(moment.Nanosecond())
	return uint64(moment.Unix()*1000000000 + milliseconds)
}

func CreateId(objectType ObjectType, method string) string {
	date := time.Now()
	return object[objectType] + strconv.Itoa(date.Year())
}

func CreateToken() string {
	b := make([]byte, 4)
	token, err := rand.Read(b)
	CheckAndLogError(err, "Token generator")
	return fmt.Sprintf("%x", token)
}
