package epos_common

import (
	"encoding/xml"
	"time"
)

type Object struct {
	XMLName    xml.Name   `xml:"object"`
	Id         uint64     `xml:"id"`
	Owner      uint64     `xml:"owner"`
	Style      Style      `xml:"style"`
	AddDate    time.Time  `xml:"added"`
	ChangeDate time.Time  `xml:"changed"`
	Shared     bool       `xml:"shared"`
	Blocked    bool       `xml:"blocked"`
	Stage      Stage      `xml:"stage"`
	Archive    Archive    `xml:"archive"`
	Attributes Attributes `xml:"attributes"`
}

type Attributes struct {
	XMLName xml.Name `xml:"attributes"`
	Images  []ImageAttribute
	Strings []StringAttribute
	Texts   []TextAttribute
	Numbers []NumberAttribute
	Dates   []DateAttribute
}

type Attribute struct {
	Id          uint64
	Title       string
	Owner       uint64
	Style       Style
	Type        AttributeType
	AddDate     time.Time
	ChangeDate  time.Time
	Description string
	Parent      uint64
	AttributeFlags
}

type AttributeType struct {
	Id            int32
	Description   string
	ModifiedTable string
	AttributeFlags
}

type AttributeFlags struct {
	Filtered     bool
	Grouped      bool
	Hierarchy    bool
	Autocomplete bool
	MultiValue   bool
	ShowInTable  bool
	UseAsHeader  bool
	RangeValue   bool
}

type ImageAttribute struct {
	Attribute
	Values []ImageValue
}

type ImageValue struct {
	Value
	URL    string
	Author uint64
	Type   ImageType
	Access AccessType
}

type ImageType struct {
	Id    int8
	Value string
	Info  string
}

type AccessType struct {
	Id    int8
	Value string
	Info  string
}

type StringAttribute struct {
	Attribute
	Values []StringValue
}

type StringValue struct {
	Value
	StringValue string
	Selected    bool
}

type TextAttribute struct {
	Attribute
	Values []TextValue
}

type TextValue struct {
	Value
	TextValue string
}

type NumberAttribute struct {
	Attribute
	Values []NumberValue
}

type NumberValue struct {
	Value
	ObjectValue uint64
	IntValue    int32
	MoneyValue  float64
	FlagValue   bool
	Rating      Rating
	Unit        string
}

type Rating struct {
	RatingStart int32
	RatingEnd   int32
}

type DateAttribute struct {
	Attribute
	Values []DateValue
}

type DateValue struct {
	Value
	DateTimeValue time.Time
	DateValue     time.Time
	TimeValue     time.Time
	RemindFlag    bool
}

type Stage struct {
	Id    int32
	Owner uint64
	Style Style
	Value string
	Info  string
}

type Archive struct {
	Id          uint64
	Owner       uint64
	Style       Style
	Value       string
	ContentPath string
	Info        string
}

type Style struct {
	Id          uint64
	Title       string
	Owner       uint64
	HeaderSize  int8
	TextSize    int8
	HeaderStyle string
	TextStyle   string
	HeaderColor string
	TextColor   string
	CardColor   string
	RowColor    string
}

type User struct {
	Id            uint64
	Username      string
	Email         string
	FirstName     string
	LastName      string
	SurName       string
	Access        AccessType
	Password      string
	BlockDate     time.Time
	SuspendDate   time.Time
	Authenticated bool
	Roles         []Role
	Auth          []Access
}

type Role struct {
	Code        string
	Title       string
	Permissions []Operation
}

type Operation struct {
	Code  uint64
	Title string
}

type Access struct {
	Token       string
	UserId      uint64
	LastLogin   time.Time
	ValidPeriod int32
}

type Value struct {
	Id        uint64
	Owner     uint64
	Attribute uint64
	Meta      string
}
