package epos_common

import "time"

type UserDao struct {
	Config     Configuration
	AccessType AccessTypeDao
	Roles      RoleDao
	Auth       AccessDao
}

func (dao *UserDao) GetList() []User {
	var commonDao = CommonDao{
		conf: dao.Config,
	}
	var userList []User
	var loggerPrefix = "UserDao/GetList"

	db := commonDao.OpenConnection()
	defer commonDao.CloseConnection(db)

	rows, err := db.Query("select id,userName,email,firstName,surName,lastName,accessLevel," +
		"encryptedPassword,blockDate,suspendDate from app_user")
	CheckAndLogError(err, loggerPrefix)
	defer func() {
		closeErr := rows.Close()
		CheckAndLogError(closeErr, loggerPrefix)
	}()

	for rows.Next() {
		user := User{}
		err := rows.Scan(&user.Id, &user.Username, &user.Email, &user.FirstName, &user.SurName, &user.LastName,
			&user.Access.Id, &user.Password, &user.BlockDate, &user.SuspendDate)
		CheckAndLogError(err, loggerPrefix)

		dao.AccessType.Config = dao.Config
		user.Access = dao.AccessType.GetItem(user.Access.Id)
		dao.Roles.Config = dao.Config
		user.Roles = dao.Roles.GetListByUser(user.Id)
		dao.Auth.Config = dao.Config
		user.Auth = dao.Auth.GetListByUser(user.Id)

		userList = append(userList, user)
	}
	return userList
}

func (dao *UserDao) GetItem(userId uint64) User {
	var commonDao = CommonDao{
		conf: dao.Config,
	}
	var user User
	var loggerPrefix = "UserDao/GetItem"

	db := commonDao.OpenConnection()
	defer commonDao.CloseConnection(db)

	row, err := db.Query("select id,userName,email,firstName,surName,lastName,accessLevel,"+
		"encryptedPassword,blockDate,suspendDate from app_user where id = $1", userId)
	CheckAndLogError(err, loggerPrefix)
	defer func() {
		closeErr := row.Close()
		CheckAndLogError(closeErr, loggerPrefix)
	}()

	err2 := row.Scan(&user.Id, &user.Username, &user.Email, &user.FirstName, &user.SurName, &user.LastName,
		&user.Access.Id, &user.Password, &user.BlockDate, &user.SuspendDate)
	CheckAndLogError(err2, loggerPrefix)

	dao.AccessType.Config = dao.Config
	user.Access = dao.AccessType.GetItem(user.Access.Id)
	dao.Roles.Config = dao.Config
	user.Roles = dao.Roles.GetListByUser(user.Id)
	dao.Auth.Config = dao.Config
	user.Auth = dao.Auth.GetListByUser(user.Id)

	return user
}

func (dao *UserDao) GetItemByName(userName string) User {
	var commonDao = CommonDao{
		conf: dao.Config,
	}
	var users []User
	var loggerPrefix = "UserDao/GetItemByName"

	db := commonDao.OpenConnection()
	defer commonDao.CloseConnection(db)

	rows, err := db.Query("select id,userName,email,firstName,surName,lastName,accessLevel,"+
		"encryptedPassword,blockDate,suspendDate from app_user where userName = $1", userName)
	CheckAndLogError(err, loggerPrefix)
	defer func() {
		closeErr := rows.Close()
		CheckAndLogError(closeErr, loggerPrefix)
	}()

	for rows.Next() {
		user := User{}
		err2 := rows.Scan(&user.Id, &user.Username, &user.Email, &user.FirstName, &user.SurName, &user.LastName,
			&user.Access.Id, &user.Password, &user.BlockDate, &user.SuspendDate)
		CheckAndLogError(err2, loggerPrefix)

		dao.AccessType.Config = dao.Config
		user.Access = dao.AccessType.GetItem(user.Access.Id)
		dao.Roles.Config = dao.Config
		user.Roles = dao.Roles.GetListByUser(user.Id)
		dao.Auth.Config = dao.Config
		user.Auth = dao.Auth.GetListByUser(user.Id)

		users = append(users, user)
	}

	if len(users) > 0 {
		return users[0]
	} else {
		return User{}
	}
}

func (dao *UserDao) CreateItem(user User) bool {
	var commonDao = CommonDao{
		conf: dao.Config,
	}
	var loggerPrefix = "UserDao/CreateItem"

	db := commonDao.OpenConnection()
	defer commonDao.CloseConnection(db)

	if user.Id == 0 {
		user.Id = CreateEposId()
	}

	result, err := db.Exec("insert into app_user values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)",
		user.Id, user.Username, user.Email, user.FirstName, user.SurName, user.LastName, user.Access.Id, user.Password,
		user.BlockDate, user.SuspendDate)
	CheckAndLogError(err, loggerPrefix)
	return CheckAffected(result, 1)
}

func (dao *UserDao) Login(userId uint64) Access {
	var commonDao = CommonDao{
		conf: dao.Config,
	}
	var access = Access{
		Token:       CreateToken(),
		UserId:      userId,
		ValidPeriod: dao.Config.ValidPeriod,
		LastLogin:   time.Now(),
	}
	var loggerPrefix = "UserDao/Login"

	db := commonDao.OpenConnection()
	defer commonDao.CloseConnection(db)

	dao.Auth.Config = dao.Config
	result := dao.Auth.CreateItem(access)
	if !result {
		CheckAndLogError(nil, loggerPrefix)
	}

	return access
}

func (dao *UserDao) UpdateItem(user User) bool {
	var commonDao = CommonDao{
		conf: dao.Config,
	}
	var loggerPrefix = "UserDao/UpdateItem"

	db := commonDao.OpenConnection()
	defer commonDao.CloseConnection(db)

	result, err := db.Exec("update app_user set userName=$1,email=$2,firstName=$3,surName=$4,"+
		"lastName=$5,accessLevel=$6,encryptedPassword=$7,blockDate=$8,suspendDate=$9 where id = $10",
		user.Username, user.Email, user.FirstName, user.SurName, user.LastName, user.Access.Id,
		user.Password, user.BlockDate, user.SuspendDate)
	CheckAndLogError(err, loggerPrefix)

	dao.Roles.Config = dao.Config
	storedRoles := dao.Roles.GetListByUser(user.Id)
	addList, deleteList := RolesDifference(storedRoles, user.Roles)
	if len(addList) > 0 {
		for i := 0; i < len(addList); i++ {
			addResult := dao.Roles.LinkItemWithUser(user.Id, addList[i].Code)
			if !addResult {
				CheckAndLogError(nil, loggerPrefix)
			}
		}
	}
	if len(deleteList) > 0 {
		for j := 0; j < len(deleteList); j++ {
			deleteResult := dao.Roles.DeleteItemFromUser(user.Id, deleteList[j].Code)
			if !deleteResult {
				CheckAndLogError(nil, loggerPrefix)
			}
		}
	}

	return CheckAffected(result, 1)
}

func (dao *UserDao) DeleteItem(userId uint64) bool {
	var commonDao = CommonDao{
		conf: dao.Config,
	}
	var loggerPrefix = "UserDao/DeleteItem"

	db := commonDao.OpenConnection()
	defer commonDao.CloseConnection(db)

	result, err := db.Exec("delete from app_user where id = $1", userId)
	CheckAndLogError(err, loggerPrefix)
	return CheckAffected(result, 1)
}
