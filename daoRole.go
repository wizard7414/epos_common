package epos_common

type RoleDao struct {
	Config     Configuration
	Operations OperationDao
}

func (dao *RoleDao) GetList() []Role {
	var commonDao = CommonDao{
		conf: dao.Config,
	}
	var rolesList []Role
	var loggerPrefix = "RoleDao/GetList"

	db := commonDao.OpenConnection()
	defer commonDao.CloseConnection(db)

	rows, err := db.Query("select code,title from app_role")
	CheckAndLogError(err, loggerPrefix)
	defer func() {
		closeErr := rows.Close()
		CheckAndLogError(closeErr, loggerPrefix)
	}()

	for rows.Next() {
		role := Role{}
		err := rows.Scan(&role.Code, &role.Title)
		CheckAndLogError(err, loggerPrefix)
		dao.Operations.Config = dao.Config
		role.Permissions = dao.Operations.GetListByRole(role.Code)
		rolesList = append(rolesList, role)
	}
	return rolesList
}

func (dao *RoleDao) GetListByUser(userId uint64) []Role {
	var commonDao = CommonDao{
		conf: dao.Config,
	}
	var rolesList []Role
	var loggerPrefix = "RoleDao/GetListByUser"

	db := commonDao.OpenConnection()
	defer commonDao.CloseConnection(db)

	rows, err := db.Query("select app_role.code,app_role.title from app_role r inner join user_role u on r.code=u.role"+
		" where u.userId=$1", userId)
	CheckAndLogError(err, loggerPrefix)
	defer func() {
		closeErr := rows.Close()
		CheckAndLogError(closeErr, loggerPrefix)
	}()

	for rows.Next() {
		role := Role{}
		err := rows.Scan(&role.Code, &role.Title)
		CheckAndLogError(err, loggerPrefix)
		dao.Operations.Config = dao.Config
		role.Permissions = dao.Operations.GetListByRole(role.Code)
		rolesList = append(rolesList, role)
	}
	return rolesList
}

func (dao *RoleDao) GetItem(roleId string) Role {
	var commonDao = CommonDao{
		conf: dao.Config,
	}
	var role Role
	var loggerPrefix = "RoleDao/GetItem"

	db := commonDao.OpenConnection()
	defer commonDao.CloseConnection(db)

	row, err := db.Query("select code,title from app_role", roleId)
	CheckAndLogError(err, loggerPrefix)
	defer func() {
		closeErr := row.Close()
		CheckAndLogError(closeErr, loggerPrefix)
	}()

	err2 := row.Scan(&role.Code, &role.Title)
	CheckAndLogError(err2, loggerPrefix)
	dao.Operations.Config = dao.Config
	role.Permissions = dao.Operations.GetListByRole(role.Code)

	return role
}

func (dao *RoleDao) CreateItem(role Role) bool {
	var commonDao = CommonDao{
		conf: dao.Config,
	}
	var loggerPrefix = "RoleDao/CreateItem"

	db := commonDao.OpenConnection()
	defer commonDao.CloseConnection(db)

	result, err := db.Exec("insert into app_role values ($1, $2)",
		role.Code, role.Title)
	CheckAndLogError(err, loggerPrefix)
	dao.Operations.Config = dao.Config
	if len(role.Permissions) > 0 {
		for i := 0; i < len(role.Permissions); i++ {
			operation := dao.Operations.GetItem(role.Permissions[i].Code)
			if operation.Title == "" {
				dao.Operations.CreateItem(role.Permissions[i])
			} else {
				dao.Operations.UpdateItem(role.Permissions[i])
			}
		}
	}
	return CheckAffected(result, 1)
}

func (dao *RoleDao) LinkItemWithUser(userId uint64, role string) bool {
	var commonDao = CommonDao{
		conf: dao.Config,
	}
	var loggerPrefix = "RoleDao/LinkItemWithUser"

	db := commonDao.OpenConnection()
	defer commonDao.CloseConnection(db)

	result, err := db.Exec("insert into user_role values ($1, $2)",
		userId, role)
	CheckAndLogError(err, loggerPrefix)
	return CheckAffected(result, 1)
}

func (dao *RoleDao) UpdateItem(role Role) bool {
	var commonDao = CommonDao{
		conf: dao.Config,
	}
	var loggerPrefix = "RoleDao/UpdateItem"

	db := commonDao.OpenConnection()
	defer commonDao.CloseConnection(db)

	result, err := db.Exec("update app_role set title=$1 where code = $2",
		role.Title, role.Code)
	CheckAndLogError(err, loggerPrefix)
	return CheckAffected(result, 1)
}

func (dao *RoleDao) DeleteItem(code string) bool {
	var commonDao = CommonDao{
		conf: dao.Config,
	}
	var loggerPrefix = "OperationDao/DeleteItem"

	db := commonDao.OpenConnection()
	defer commonDao.CloseConnection(db)

	result, err := db.Exec("delete from app_role where code = $1", code)
	CheckAndLogError(err, loggerPrefix)
	return CheckAffected(result, 1)
}

func (dao *RoleDao) DeleteItemFromUser(userId uint64, role string) bool {
	var commonDao = CommonDao{
		conf: dao.Config,
	}
	var loggerPrefix = "OperationDao/DeleteItemFromUser"

	db := commonDao.OpenConnection()
	defer commonDao.CloseConnection(db)

	result, err := db.Exec("delete from user_role where userId = $1 and role = $2", userId, role)
	CheckAndLogError(err, loggerPrefix)
	return CheckAffected(result, 1)
}
