package epos_common

import (
	"database/sql"
	"log"
)

/* Simple error handler
 */
func SimpleErrorCheck(err error) {
	if err != nil {
		panic(err)
	}
}

/* Logging error handler
 */
func CheckAndLogError(err error, message string) {
	if err != nil {
		if message != "" {
			log.Fatal(message, err)
		} else {
			log.Fatal(err)
		}
	}
}

func CheckAffected(result sql.Result, expected int64) bool {
	affected, err := result.RowsAffected()
	if err != nil {
		CheckAndLogError(err, "invalid result format")
		return false
	} else {
		if affected == expected {
			return true
		} else {
			CheckAndLogError(nil, "Error, unexpected rows modification")
			return false
		}
	}
}
